import { html, LitElement } from 'lit-element'
import '@vaadin/vaadin-date-picker'

class PeriodPicker extends LitElement {
  render() {
    return html`
      <style>
        /** TEST1: NOT WORKING (neither user-background or 'green') */
        vaadin-date-picker {
          --lumo-primary-color: var(--custom-background, green);
        }
      </style>

      <!-- TEST 2: NOT WORKING (user-background is not used) -->
      <dom-module
        id="custom-vaadin-date-picker"
        theme-for="vaadin-date-picker-overlay-content"
      >
        <template>
          <style>
            :host([theme='custom']) {
              --lumo-primary-color: var(--custom-background, red);
              --lumo-primary-text-color: var(--custom-background, red);
            }
          </style>
        </template>
      </dom-module>

      <vaadin-date-picker theme="${this.theme}"></vaadin-date-picker>
    `
  }

  static get properties() {
    return {
      theme: String,
    }
  }
}

customElements.define('period-picker', PeriodPicker)
